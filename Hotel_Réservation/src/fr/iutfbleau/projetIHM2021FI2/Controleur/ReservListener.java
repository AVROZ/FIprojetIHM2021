package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;

import javax.swing.JOptionPane;

import fr.iutfbleau.projetIHM2021FI2.MP.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;

/**
 * Listener du boutton Réserver qui déclenche l'enregistrement de la réservation
 */
public class ReservListener implements ActionListener {
	private ReservationFactoryMP hotel;
	private FicheRecap recap;

	/**
	 * Construteur qui prend 2 paramètres :
	 * @param hotel
	 * @param rec
	 */
	public ReservListener(ReservationFactoryMP hotel, FicheRecap rec) {
		this.hotel = hotel;
		this.recap = rec;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		try {
			this.hotel.createReservation(recap.getPrereservation(), recap.getChambre());
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}

		JOptionPane.showMessageDialog(null, "La chambre a bien été réservé !", "Réservé", JOptionPane.INFORMATION_MESSAGE);
	}
}