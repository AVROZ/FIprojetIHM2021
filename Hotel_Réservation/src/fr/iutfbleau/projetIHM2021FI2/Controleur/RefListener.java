package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import javax.swing.JTextField;

/**
 * Listener qui efface le texte de la case Référence quand elle gagne le focus et le remet lorsque
 * le focus est perdu
 */
public class RefListener implements FocusListener {

    @Override
    public void focusGained(FocusEvent e) {
        JTextField reference = (JTextField) e.getSource();
        if (reference.getText().equals("Référence")) {
            reference.setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        JTextField reference = (JTextField) e.getSource();
        if (reference.getText().equals("")) {
            reference.setText("Référence");
        }
    }

}
