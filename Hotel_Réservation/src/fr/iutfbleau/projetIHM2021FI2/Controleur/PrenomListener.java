package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import javax.swing.JTextField;

/**
 * Listener qui efface le texte de la case Prénom quand elle gagne le focus et le remet lorsque
 * le focus est perdu
 */
public class PrenomListener implements FocusListener {

    @Override
    public void focusGained(FocusEvent e) {
        JTextField prenom = (JTextField) e.getSource();
        if (prenom.getText().equals("Prénom")) {
            prenom.setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        JTextField prenom = (JTextField) e.getSource();
        if (prenom.getText().equals("")) {
            prenom.setText("Prénom");
        }
    }

}
