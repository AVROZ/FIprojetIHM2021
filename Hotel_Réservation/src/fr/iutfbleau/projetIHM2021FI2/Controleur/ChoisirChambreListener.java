package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;

public class ChoisirChambreListener implements ActionListener {

	private Integer[] listeChambres;
	private Integer chambreChoisie;
	private JLabel chambreAll;
	private JPanel recap;
	private ReservationFactory hotel;
	private Prereservation preresa;

	public ChoisirChambreListener(ReservationFactory factory, Integer[] listeChambres, Integer chambreChoisie, JLabel chambreAll, JPanel recap, Prereservation pre) {
		this.hotel = factory;
		this.listeChambres = listeChambres;
		this.chambreChoisie = chambreChoisie;
		this.chambreAll = chambreAll;
		this.recap = recap;
		this.preresa = pre;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Set<Chambre> chambres = null;

		try {
			chambres = this.hotel.getChambres(this.preresa);
		} catch(Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
		ArrayList<Integer> list = new ArrayList<>();
		for(Chambre chambre:chambres) {
			list.add(chambre.getNumero());
		}

		Collections.sort(list);
		listeChambres = list.toArray(new Integer[list.size()]);
		chambreChoisie = (Integer) JOptionPane.showInputDialog(null, "Quelle chambre voulez-vous allouer ?", "Choisir une chambre", JOptionPane.QUESTION_MESSAGE, null, listeChambres, listeChambres[0]);
		if (chambreChoisie != null) {
			chambreAll.setText("Chambre Allouée : " + String.valueOf(chambreChoisie));
			recap.repaint();
		}
	}

	public void setPrereservation(Prereservation pre) {
		this.preresa = pre;
	}
}