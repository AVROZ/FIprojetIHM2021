package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import javax.swing.JTextField;

/**
 * Listener qui efface le texte de la case Nom quand elle gagne le focus et le remet lorsque
 * le focus est perdu
 */
public class NomListener implements FocusListener {

    @Override
    public void focusGained(FocusEvent e) {
        JTextField nom = (JTextField) e.getSource();
        if (nom.getText().equals("Nom")) {
            nom.setText("");
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        JTextField nom = (JTextField) e.getSource();
        if (nom.getText().equals("")) {
            nom.setText("Nom");
        }
    }

}
