package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;

import fr.iutfbleau.projetIHM2021FI2.MP.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;

/**
 * Listener du boutton quitter qui ferme les connexions à la base de donnée avant de fermer
 * l'application
 */
public class QuitListener implements ActionListener {
	private PrereservationFactoryMP booking;
	private ReservationFactoryMP hotel;

	/**
	 * Constructeur qui prend 2 factory afin de fermer les connexions aux bases de données
	 * @param booking
	 * @param hotel
	 */
	public QuitListener(PrereservationFactoryMP booking, ReservationFactoryMP hotel) {
		this.booking = booking;
		this.hotel = hotel;
	}

    @Override
    public void actionPerformed(ActionEvent arg0) {
		try {
			this.booking.getBD().closeConnection();
			this.hotel.getBD().closeConnection();
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
        System.exit(0);
    }
    
}
