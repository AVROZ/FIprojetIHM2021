package fr.iutfbleau.projetIHM2021FI2.Controleur;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;
import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.MP.*;

import java.time.LocalDate;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * Listener s'occupant de savoir si une ligne du tableau est sélectionné
 */
public class RowListener implements ListSelectionListener {

	String refStr;
	String dateStr;
	String joursStr;
	String typeChambreStr;
	String idStr;
	String prenomStr;
	String nomStr;

	private JTable table;
	//private BottomContainer botContainer;
	//private HashMap<String, String> infos;

	private FicheRecap recap;

	private int selectedRow;
	
	private List<String> rowContent;
	private Iterator<String> itr;


	/**
	 * Constructeur ne faisant rien de spécial
	 * @param table
	 * @param rec
	 */
	public RowListener(JTable table, FicheRecap rec) {

		this.table = table;
		
		this.recap = rec;

		this.rowContent = new ArrayList<>();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// getValueIsAdjusting() sert à faire une seule execution
		if (!e.getValueIsAdjusting()) {
			// Reset de la List
			rowContent.clear();

			selectedRow = this.table.getSelectedRow();

			for (int i = 0; i < 7; i++) {
				rowContent.add((String) table.getValueAt(selectedRow, i));
			}

			addNewDataAndRefresh();
		}
	}

	/**
	 * Met à jour les infos de la fiche récapitulative à partir de la ligne sélectionné
	 */
	public void addNewDataAndRefresh() {
		itr = rowContent.iterator();

		if (itr.hasNext()) {
			refStr = itr.next();
		}
		if (itr.hasNext()) {
			dateStr = itr.next();
		}
		if (itr.hasNext()) {
			joursStr = itr.next();
		}
		if (itr.hasNext()) {
			typeChambreStr = itr.next();
		}
		if (itr.hasNext()) {
			idStr = itr.next();
		}
		if (itr.hasNext()) {
			prenomStr = itr.next();
		}
		if (itr.hasNext()) {
			nomStr = itr.next();
		}

		recap.getRef().setText("Référence : " + refStr);
		recap.getDate().setText("Date : " + dateStr);
		recap.getJours().setText("Jours : " + joursStr);
		recap.getTypeChambre().setText("Type Chambre : " + typeChambreStr);
		recap.getId().setText("ID : " + idStr);
		recap.getPrenomClient().setText("Prénom : " + prenomStr);
		recap.getNomClient().setText("Nom : " + nomStr);

        Chambre chambre = null;
		Prereservation preresa = PrereservationFactoryMP.getPrereservationByString(refStr, dateStr, joursStr, typeChambreStr, idStr, prenomStr, nomStr);
		try {
			chambre = this.recap.getHotel().getChambre(preresa);
		} catch(Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}

		recap.getChambreAllouee().setText("Chambre Allouée : "+ String.valueOf(chambre.getNumero()));

		recap.setPrereservation(preresa);
		recap.getRecap().repaint();
	}
	
}
