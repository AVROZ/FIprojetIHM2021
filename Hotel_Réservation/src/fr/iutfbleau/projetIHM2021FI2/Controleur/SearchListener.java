package fr.iutfbleau.projetIHM2021FI2.Controleur;

import java.awt.event.*;
import java.time.LocalDate;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.MP.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;

/**
 * Listener du boutton Chercher qui affiche dans le tableau toutes les préréservations
 * recherché dans la base de donnée à partir du Prénom et Nom ou de la Référence
 */
public class SearchListener implements ActionListener {

	private JTextField prenomField;
	private JTextField nomField;
	private JTextField refField;
	private DefaultTableModel model;
	private JTable table;
	private Prereservation preRes;
	private Set<Prereservation> setPreres;
	private PrereservationFactory booking;
	// private Set<Prereservation> preResList;

	// String prenom;
	// String nom;
	// String Reference;

	/**
	 * Constructeur ne faisant rien de spécial
	 * @param factory
	 * @param prenomField
	 * @param nomField
	 * @param refField
	 * @param model
	 * @param table
	 */
	public SearchListener(PrereservationFactory factory, JTextField prenomField, JTextField nomField, JTextField refField, DefaultTableModel model, JTable table) {
		this.booking = factory;
		this.prenomField = prenomField;
		this.nomField = nomField;
		this.refField = refField;
		this.model = model;
		this.table = table;

		setPreres = new HashSet<Prereservation>();

		// setPreres.add(new PrereservationMP("4751-3708-LRMF", LocalDate.of(2018, 01, 05), 1, TypeChambre.UNLD, new ClientMP(1, "Marine", "Carpentier")));
		// setPreres.add(new PrereservationMP("2436-8461-NXLL", LocalDate.of(2018, 04, 10), 3, TypeChambre.UNLD, new ClientMP(1, "Marine", "Carpentier")));
		// setPreres.add(new PrereservationMP("0564-7481-EKLG", LocalDate.of(2018, 04, 10), 7, TypeChambre.UNLD, new ClientMP(2, "Charles", "Leclerc")));
		// setPreres.add(new PrereservationMP("4561-7524-RTID", LocalDate.of(2018, 04, 11), 7, TypeChambre.UNLD, new ClientMP(3, "Fernando", "Alonso")));
		// setPreres.add(new PrereservationMP("5441-3285-ZOPF", LocalDate.of(2018, 05, 10), 15, TypeChambre.UNLD, new ClientMP(4, "Pierre", "Chabrier")));
		// setPreres.add(new PrereservationMP("2436-8461-BOXN", LocalDate.of(2018, 05, 10), 16, TypeChambre.UNLD, new ClientMP(5, "Sylvain", "Levy")));
		// setPreres.add(new PrereservationMP("0564-7481-RTHV", LocalDate.of(2018, 11, 25), 4, TypeChambre.UNLD, new ClientMP(6, "Jean", "Jacques")));
		// setPreres.add(new PrereservationMP("7110-5467-SPBN", LocalDate.of(2018, 12, 3), 3, TypeChambre.UNLD, new ClientMP(7, "Georges", "Russel")));

		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// System.out.println("Recherche...");
		// System.out.println("Prénom = " + prenomField.getText() + ", Nom = " + nomField.getText() + ", Numéro de Réservation = " + refField.getText());
		
		table.clearSelection();
		clearTable();

		if (!this.prenomField.getText().equals(new String("Prénom"))  &&  !this.nomField.getText().equals(new String("Nom"))) {
			try {
			    this.setPreres = this.booking.getPrereservations(this.nomField.getText(), this.prenomField.getText());
            }
			catch(Exception e1) {
				FenetreErreur fenErr = new FenetreErreur(e1);
			}
		} else if (!this.refField.getText().equals("Référence")) {
			try {
				Set<Prereservation> preresa = new HashSet<>();
			    preresa.add(this.booking.getPrereservation(this.refField.getText()));
				this.setPreres = preresa;
			} catch(Exception e2) {
				FenetreErreur fenErr = new FenetreErreur(e2);
			}
		} else if (!this.prenomField.getText().equals("Prénom")  &&  !this.nomField.getText().equals("Nom")  &&  !this.refField.getText().equals("Référence")) {
			try {
			    this.setPreres = this.booking.getPrereservations(this.nomField.getText(), this.prenomField.getText());
            }
			catch(Exception e1) {
				FenetreErreur fenErr = new FenetreErreur(e1);
			}
		} else {
			FenetreErreur fenErr = new FenetreErreur(new Exception("Veuillez rentrer soit un nom, soit une référence"));
		}

		tableConstructor();
	}

	/**
	 * Retire toutes les lignes déjà présentes
	 */
	void clearTable() {
		//model.getDataVector().removeAllElements();
		int rowCount = model.getRowCount();
		for (int i = rowCount - 1; i >= 0; i--) {
			model.removeRow(i);
		}
	}

	/**
	 * Un constructeur de lignes pour JTable
	 * @param preres
	 * @return
	 */
	public Object[] rowConstructor(Prereservation preres) {
		String[] row = new String[] {preres.getReference(), preres.getDateDebut().toString(), String.valueOf(preres.getJours()),
									 preres.getTypeChambre().name(), String.valueOf(preres.getClient().getId()),
									 String.valueOf(preres.getClient().getPrenom()), String.valueOf(preres.getClient().getNom())};
		
		return row;
	}

	/**
	 * Un constructeur de tableau utilisant un constructeur de ligne
	 */
	public void tableConstructor() {
		for (Prereservation pre : setPreres) {
			model.addRow(rowConstructor(pre));
		}
	}

	// public void Requete() {
	//     preRes = new PreReservationBD();
		
	//     preResList = preRes.getPrereservations(nomField.getText(), prenomField.getText());
	//     if (preResList.isEmpty()) {
	//         preResList = preRes.getPrereservation(refField.getText());
	//     }
	// }

	// public void setPrenom(String prenom) {
	//     this.prenom = prenom;
	// }

	// public void setNom(String nom) {
	//     this.nom = nom;
	// }

	// public void setReference(String Reference) {
	//     this.Reference = Reference;
	// }
	
}
