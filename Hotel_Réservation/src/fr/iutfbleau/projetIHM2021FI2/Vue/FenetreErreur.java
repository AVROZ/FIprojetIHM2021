package fr.iutfbleau.projetIHM2021FI2.Vue;

import javax.swing.JOptionPane;

/**
 * Classe utiliser pour former n'importe quel erreur du programme. Elle affiche une fenêtre popup
 * qui alerte de l'erreur
 */
public class FenetreErreur {

    String errorMsg;

    /**
     * Constructeur qui affiche la fenêtre popup avec le contenu de l'erreur
     * @param e
     */
    public FenetreErreur(Exception e) {
        errorMsg = e.getMessage();

        JOptionPane.showMessageDialog(null, errorMsg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void main(String[] args) {
        try {
            FenetreErreur.A();
        } catch (Exception e) {
            FenetreErreur fenetre = new FenetreErreur(e);
        }
        
        // try {
        //     FenetreErreur.B();
        // } catch (Exception e) {
        //     FenetreErreur fenetre = new FenetreErreur(e);
        // }
    }

    public static void A() {
        throw new IllegalArgumentException("Accès à la base de donnée non autorisé");
    }

    public static void B() {
        throw new IllegalStateException("Le type de retour ne peut pas être converti dans le type voulu");
    }
}