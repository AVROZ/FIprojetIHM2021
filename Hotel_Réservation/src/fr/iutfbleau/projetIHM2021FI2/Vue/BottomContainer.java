package fr.iutfbleau.projetIHM2021FI2.Vue;
import javax.swing.*;
import javax.swing.table.*;

import fr.iutfbleau.projetIHM2021FI2.Controleur.*;
import fr.iutfbleau.projetIHM2021FI2.MP.PrereservationFactoryMP;
import fr.iutfbleau.projetIHM2021FI2.MP.ReservationFactoryMP;

import java.awt.*;

/**
 * Classe s'occupant de la partie basse de la fenêtre, autrement dit du tableau, de la fiche
 * récapitulative, du bouton Réserver et du boutton quitter
 */
public class BottomContainer extends JPanel {

	private JTable table;
	// HashMap ou sous-classe
	private FicheRecap recap;

	// listeChambres à remplir avec la requête

	private JButton quitter;
	private JButton reserver;

	private DefaultTableModel model;

	/**
	 * Contructeur qui met en forme la partie basse de la fenêtre uniquement afin d'être
	 * implémenté dans la fenêtre principale
	 * @param booking
	 * @param hotel
	 */
	public BottomContainer(PrereservationFactoryMP booking, ReservationFactoryMP hotel) {

		
		this.quitter = new JButton("Quitter");
		this.reserver = new JButton("Réserver");

		this.setLayout(new GridBagLayout());
		this.setPreferredSize(new Dimension(350, 320));

		this.recap = new FicheRecap(hotel);
		

		// Panneau récapitulatif des informations de la chambre
		
		//recap.setBorder(new Border());
		

		// Mise en forme du tableau
		// String[] columnNames = {"Reference",
		// 						"Date",
		// 						"Jours",
		// 						"Type Chambre",
		// 						"ID",
		// 						"Prenom",
		// 						"Nom"};

		// Object[][] data = {
		// 	{"4751-3708-LRMF", "2018-01-05", 1, "UNLD", 1, "Marine", "Carpentier"},
		// 	{"2436-3909-NXLL", "2018-01-07", 1, "UNLS", 1, "Marine", "Carpentier"}
		// };

		this.table = new JTable(model) {
			@Override
			public boolean editCellAt(int row, int column, java.util.EventObject e) {
				return false;
			}
		};

		// Mise en forme du tableau
		model = (DefaultTableModel) table.getModel();
		model.addColumn("Reference");
		model.addColumn("Date");
		model.addColumn("Jours");
		model.addColumn("Type Chambre");
		model.addColumn("ID");
		model.addColumn("Prenom");
		model.addColumn("Nom");
		
		//getFocusedComponent
		table.getTableHeader().setReorderingAllowed(false);
		table.setPreferredSize(new Dimension(700, 250));
		table.getTableHeader().setPreferredSize(new Dimension(700, 40));
		//table.setCellSelectionEnabled(false);
		//table.setColumnSelectionAllowed(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		//table.setDragEnabled(false);
		
		// Ajout des listeners
		table.getSelectionModel().addListSelectionListener(new RowListener(table, recap));
		quitter.addActionListener(new QuitListener(booking, hotel));
		reserver.addActionListener(new ReservListener(hotel, recap));

		// Reste de la fenêtre
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;      // la plage de cellules commence à la première colonne
		gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 2;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.HORIZONTAL;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.NORTHWEST; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 0, 5);    // laisse 5 pixels de vide autour du composant
		this.add(table.getTableHeader(), gbc);

		gbc.gridx = 0;      // la plage de cellules commence à la première colonne
		gbc.gridy = 1;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 2;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(0, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(table, gbc);

		gbc.gridx = 2;      // la plage de cellules commence à la première colonne
		gbc.gridy = 0;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 3; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.BOTH;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(recap.getRecap(), gbc);

		gbc.gridx = 0;      // la plage de cellules commence à la première colonne
		gbc.gridy = 2;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.NONE;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(quitter, gbc);

		gbc.gridx = 1;      // la plage de cellules commence à la première colonne
		gbc.gridy = 2;      // la plage de cellules commence à la deuxième ligne
		gbc.gridwidth = 1;  // la plage de cellules englobe deux colonnes
		gbc.gridheight = 1; // la plage de cellules englobe une seule ligne
		gbc.fill = GridBagConstraints.NONE;     // n'occupe pas tout l'espace de la plage
		gbc.anchor = GridBagConstraints.CENTER; // se place au centre de la plage
		gbc.weightx = 1.0;  // souhaite plus de largeur si possible
		gbc.weighty = 1.0;  // n'a pas besoin de hauteur supplémentaire
		gbc.insets = new Insets(5, 5, 5, 5);    // laisse 5 pixels de vide autour du composant
		this.add(reserver, gbc);
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public JTable getTable() {
		return table;
	}
}