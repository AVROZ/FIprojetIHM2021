package fr.iutfbleau.projetIHM2021FI2.Vue;

import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.Controleur.*;
import fr.iutfbleau.projetIHM2021FI2.MP.*;

import java.awt.*;
import javax.swing.*;

/**
 * Classe chargé de contenir toutes les infos de la fiche récapitulative
 */
public class FicheRecap {

	private final String typeChambreStr = "Type Chambre : ";
	private final String chambreAllStr = "Chambre Allouée : ";

	private JPanel recap;
	private JLabel ref;
	private JLabel date;
	private JLabel jours;
	private JLabel typeChambre;
	private JLabel id;
	private JLabel prenomClient;
	private JLabel nomClient;
	private JLabel chambreAll;

	private Integer[] listeChambres;
	private Integer chambreChoisie;
	private JButton choisirChambre;

	private ReservationFactory hotel;
	
	private Prereservation preresa;

	private ChoisirChambreListener chambreListener;

	/**
	 * Constructeur qui ajoute toutes les lignes de la fiche récapitulative
	 * @param hotel
	 */
	public FicheRecap(ReservationFactory hotel) {
		this.recap = new JPanel();
		this.ref = new JLabel("Référence : ");
		this.date = new JLabel("Date : ");
		this.jours = new JLabel("Jours : ");
		this.typeChambre = new JLabel(typeChambreStr);
		this.id = new JLabel("ID : ");
		this.prenomClient = new JLabel("Prénom : ");
		this.nomClient = new JLabel("Nom : ");
		this.chambreAll = new JLabel(chambreAllStr);
		this.hotel = hotel;
		this.preresa = null;

		this.listeChambres = null;

		this.choisirChambre = new JButton("Choisir une chambre");
		this.chambreListener = new ChoisirChambreListener(hotel, listeChambres, chambreChoisie, chambreAll, this.recap, this.preresa);
		this.choisirChambre.addActionListener(chambreListener);

		recap.setLayout(new GridLayout(9, 1));
		recap.setPreferredSize(new Dimension(250, 300));

		this.recap.add(ref);
		this.recap.add(date);
		this.recap.add(jours);
		this.recap.add(typeChambre);
		this.recap.add(id);
		this.recap.add(prenomClient);
		this.recap.add(nomClient);
		this.recap.add(chambreAll);
		this.recap.add(choisirChambre);
	}

	public JPanel getRecap() {
		return this.recap;
	}

	public JLabel getRef() {
		return this.ref;
	}

	public JLabel getDate() {
		return this.date;
	}

	public JLabel getJours() {
		return this.jours;
	}

	public JLabel getTypeChambre() {
		return this.typeChambre;
	}

	public JLabel getId() {
		return this.id;
	}

	public JLabel getPrenomClient() {
		return this.prenomClient;
	}

	public JLabel getNomClient() {
		return this.nomClient;
	}

	public JLabel getChambreAllouee() {
		return this.chambreAll;
	}

	public Chambre getChambre() {
		int numChambre = Integer.parseInt(this.chambreAll.getText().substring(chambreAllStr.length()));
		return new ChambreMP(numChambre, ChambreMP.getTypeChambreParseString(this.typeChambre.getText().substring(typeChambreStr.length())));
	}

	public ReservationFactory getHotel() {
		return this.hotel;
	}

	public Prereservation getPrereservation() {
		return this.preresa;
	}

	public void setPrereservation(Prereservation pre) {
		this.preresa = pre;
		this.chambreListener.setPrereservation(pre);
	}
}