package fr.iutfbleau.projetIHM2021FI2.MP;
import fr.iutfbleau.projetIHM2021FI2.API.*;
import java.util.*;

/**
 * Une chambre
 */
public class ChambreMP implements Chambre{

	private int numero;
	private TypeChambre type;

	/**
	 * Constructeur de la chambre
	 * @param numero numero de la chambre (correspond au numéro dans l'hôtel)
	 * @param t type de la chambre (voir ENUM TypeChambre)
	 * @throws NullPointerException si le type de la chambre est null
	 */
	public ChambreMP(int numero, TypeChambre t){
		Objects.requireNonNull(t,"On ne peut pas créer une chambre avec un type de chambre à null.");
		this.numero=numero;
		this.type=t;
	}

	/**
	 * permet de récupérer le numéro de la chambre.
	 * @return le numéro.
	 */
	public int getNumero(){
		return this.numero;
	}
	

	/**
	 * permet de savoir si la chambre a un seul lit qui est simple
	 * @return vrai si c'est le cas.
	 */
	@SuppressWarnings("deprecation")
	public boolean unLitSimple(){
		//https://stackoverflow.com/questions/1750435/comparing-java-enum-members-or-equals
		return (this.type == TypeChambre.UNLS);
	}

	/**
	 * permet de savoir si la chambre a deux lits simples
	 * @return vrai si c'est le cas.
	 */
	@SuppressWarnings("deprecation")
	public boolean deuxLitsSimples(){
		return (this.type == TypeChambre.DEUXLS);
	}

	/**
	 * permet de savoir si la chambre a un lit double
	 * @return vrai si c'est le cas.
	 */
	@SuppressWarnings("deprecation")
	public boolean unLitDouble(){
		return (this.type == TypeChambre.UNLD);
	}


	/**
	 * @return le type de chambre (un type énuméré de l'API)
	 *
	 * NB. Les trois méthodes ci-dessus sont assez moches.
	 * De toute façon Chambre ou Prérerservation exposent le type énuméré TypeChambre à la vue.
	 * Il est donc plus simple d'ajouter des types de chambre à ce type énuméré plutôt que d'ajouter des tests ici.
	 * Je laisse les méthodes obsolètes pour illustrer l'annotation <pre>@deprecated</pre> de la javadoc.
	 */
	public TypeChambre getType(){
		return this.type;
	}

	/**
	 * Retourne le TypeChambre correspondant à l'identifiant fourni
	 * @param nb numéro de catégorie de la chambre
	 * @return TypeChambre correspond à cet identifiant
	 */
	public static TypeChambre getTypeChambreById(int nb) {
		switch(nb) {
			case 1:  return TypeChambre.UNLS;
			case 2:  return TypeChambre.UNLD;
			case 3:  return TypeChambre.DEUXLS;
			default: return null;
		}
	}

	/**
	 * Retourne l'identifiant correspondant au TypeChambre fourni
	 * @param type type de la chambre
	 * @return identifiant correspond à ce type
	 */
	public static int getIdByTypeChambre(TypeChambre type) {
		switch(type) {
			case UNLS:   return 1;
			case UNLD:   return 2;
			case DEUXLS: return 3;
			default: return -1;
		}
	}

	/**
	 * Retourne le typede chambre correspondant à la chaîne de caractères
	 * @param str chaînes de caractères à convertir
	 * @return type chambre correspondant
	 */
	public static TypeChambre getTypeChambreParseString(String str) {
		switch(str) {
			case "UNLS":   return TypeChambre.UNLS;
			case "UNLD":   return TypeChambre.UNLD;
			case "DEUXLS": return TypeChambre.DEUXLS;
			default: return null;
		}
	}

	// voir MonPrint dans l'interface Chambre
	// @Override
	// public String toString() {
	//     return String.format("Chambre " + this.numero + " ("+ this.type +")");
	// }
	
}
