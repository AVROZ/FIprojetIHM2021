package fr.iutfbleau.projetIHM2021FI2.MP;

import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.BD.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.FenetreErreur;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

/**
 * Usine persistante stockant les préréservation dans une structure de donnée permettant de simuler un ensemble.
 *
 * Elle permet de rechercher les préréservations à partir du numéro d'une préréservation, ou bien du nom et prenom d'un client.
 *
 * La recherche par numéro devrait être plus efficace.
 *
 */
public class PrereservationFactoryMP implements PrereservationFactory{
	
	private PreReservationBD bd; 

	/**
	 * Constructeur qui créé la classe de gestion de la base de données externe
	 * @throws SQLException si la connexion à la base de données a échoué.
	 * @throws ClassNotFoundException si le driver de mariadb n'a pas été trouvé.
	 */
	public PrereservationFactoryMP() throws ClassNotFoundException, SQLException {
		this.bd = new PreReservationBD();
	}

	/**
	 * Recherche une préréservation par reference
	 * @param  r la référence du système de préréservation 
	 * @return la préréservation.
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalStateException si la Préréservation avec cette référence n'existe pas.
	 * @throws SQLException si l'accès à la base de données externe est impossible.
	 *
	 * Ne devrait pas retourner un objet null.
	 */
	public Prereservation getPrereservation(String r) {
		Objects.requireNonNull(r,"La référence recherchée est null.");
		Prereservation preresa = null;
		try {
			preresa = this.bd.getPrereservation(r);
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
		return preresa;
	}

	/**
	 * Recherche une préréservation par nom et prenom
	 * @param  n le nom
	 * @param  p le prenom
	 * @return un ensemble de préréservations.
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalStateException si aucune préréservation n'existe avec ce nom
	 * @throws SQLException si l'accès à la base de données externe est impossible.
	 *
	 * Ne devrait pas retourner un objet null ou un ensemble vide.
	 */
	public Set<Prereservation> getPrereservations(String n, String p){
		Objects.requireNonNull(n,"Le nom recherché est null.");
		Objects.requireNonNull(p,"Le prénom recherché est null.");
		Set<Prereservation> preresa = null;
		try {
			preresa = this.bd.getPrereservations(n, p);
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
		return preresa;
	}

	/**
	 * Renvoie une prereservation construite avec les données fournies en String
	 * @return 
	 */
	public static Prereservation getPrereservationByString(String reference, String dateStr, String joursStr, String typeStr, String idClientStr, String prenomClient, String nomClient) {
		System.out.println("Valeur date dans la méthode : " + dateStr);
		LocalDate dateDebut = LocalDate.parse(dateStr);
		int jours = Integer.parseInt(joursStr);
		TypeChambre type = ChambreMP.getTypeChambreParseString(typeStr);
		Client client = new ClientMP(Integer.parseInt(idClientStr), prenomClient, nomClient);
		Prereservation preresa = new PrereservationMP(reference, dateDebut, jours, type, client);
		return preresa;
	}

	/**
	 * Retourne le gestionnaire de la base de données externe associée à la Factory
	 * @return le gestionnaire de la base de données externe
	 */
	public PreReservationBD getBD() {
		return this.bd;
	}
}