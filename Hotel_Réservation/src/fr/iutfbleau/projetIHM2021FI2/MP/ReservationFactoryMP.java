package fr.iutfbleau.projetIHM2021FI2.MP;
import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.BD.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;

import java.time.LocalDate;
import java.util.*;

/**
 * Usine persistante stockant les réservations dans une structure de données permettant de simuler un ensemble.
 *
 * Par paresse, la réservation possède la même référence que la préréservation correspondante.
 *
 */
public class ReservationFactoryMP implements ReservationFactory{

	private ReservationBD bd;

	/**
	 * Constructeur qui créé la classe de gestion de la base de données interne
	 */
	public ReservationFactoryMP(){
		this.bd = new ReservationBD();
	}
	
	/**
	 * Recherche une chambre adéquate à partir de
	 * @param  p une  préréservation 
	 * @return la chambre
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalStateException si une chambre correspondant à cette Préréservation n'existe pas.
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 *
	 * Ne devrait pas retourner un objet null.
	 */
	public Chambre getChambre(Prereservation p) {
		Objects.requireNonNull(p,"La préréservation est nulle.");
		int numero;
		Chambre c = null;
		try {
			numero = this.bd.getOneFreeRoom(p);
			c = new ChambreMP(numero,p.getTypeChambre());
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
		return c;
	}

	/**
	 * Recherche toutes les chambres adéquates à partir de
	 * @param  p une  préréservation 
	 * @return les chambres (set de chambre)
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalStateException si une chambre correspondant à cette Préréservation n'existe pas.
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 *
	 * Ne devrait pas retourner un objet null.
	 */
	public Set<Chambre> getChambres(Prereservation p){
		Objects.requireNonNull(p,"La préréservation est null.");
		ArrayList<Integer> room_ids = null;
		try {
			room_ids = this.bd.getFreeRoom(p);
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
		Set<Chambre> chambres = new HashSet<>();
		for(int id:room_ids) {
			chambres.add(new ChambreMP(id, p.getTypeChambre()));
		}
		return chambres;
	}
	
	/**
	 * Fabrique (ajoute) une réservation
	 * @param  p une  préréservation 
	 * @param  c une  chambre (normalement libre et adaptée à la préréservation) 
	 * @return la réservation
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalArgumentException si la chambre ne correspondant pas au type de chambre de la préréservation.
	 * @throws IllegalStateException si la chambre n'est pas disponible.
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 *
	 * Ne devrait pas retourner un objet null.
	 */    
	public Reservation createReservation(Prereservation p, Chambre c){
		Objects.requireNonNull(p,"La préréservation est null.");
		Objects.requireNonNull(p,"La chambre est null.");
		if (c.getType()!=p.getTypeChambre()) {
			throw new IllegalArgumentException("Erreur sur le type de la chambre: la préréservation indique " + p.getTypeChambre() + " mais la chambre est  " + c.getType());
		} else {
			Reservation r = new ReservationMP(p.getReference(), p.getDateDebut(), p.getJours(), c, p.getClient());
			try {
				this.bd.createReservation(r);
			} catch (Exception e) {
				FenetreErreur fenErr = new FenetreErreur(e);
			}
			return r;
		}
	}

	/**
	 * Retourne le gestionnaire de la base de données interne associée à la Factory
	 * @return le gestionnaire de la base de données interne
	 */
	public ReservationBD getBD() {
		return this.bd;
	}

	///**
	// * Cherche les réservations
	// * @param  d une date
	// * @return la ou les réservation(s) à cette date sous forme d'un ensemble
	// * @throws NullPointerException si un argument est null
	// *
	// * Ne devrait pas retourner un objet null, par contre peut être un ensemble qui est vide.
	// */    
	/*public Set<Reservation> getReservation(LocalDate d){
		Objects.requireNonNull(d,"La date proposée est null.");
		
		Set<Reservation> s;
		if (this.brain.containsKey(d)){
			s = this.brain.get(d);
			}
		else{
			s = new HashSet<Reservation>();
		}
		return s;
	}*/

	/**
	 * Cherche le nombre de chambres disponibles pour une date (réservées ou non).
	 * @param  d une date
	 * @return un entier
	 * @throws NullPointerException si un argument est null
	 *
	 * Ne devrait pas retourner un entier négatif.
	 */    
	public int getDisponibles(LocalDate d){
		throw new UnsupportedOperationException("pas encore implanté");
	}

	/**
	 * Cherche les réservations
	 * @param  d une date
	 * @param  t un type de chambre
	 * @return la ou les réservation(s) pour ce type de chambre à cette date sous forme d'un ensemble
	 * @throws NullPointerException si un argument est null
	 *
	 * Ne devrait pas retourner un objet null, par contre peut être un ensemble qui est vide.
	 */    
	public Set<Reservation> getReservation(LocalDate d, TypeChambre t){
		throw new UnsupportedOperationException("pas encore implanté");
	}
	
	/**
	 * Cherche le nombre de chambres disponibles d'un certain type pour une date (réservées ou non).
	 * @param  d une date
	 * @param  t un type de chambre
	 * @return un entier
	 * @throws NullPointerException si un argument est null
	 *
	 * Ne devrait pas retourner un entier négatif.
	 */    
	public int getDisponibles(LocalDate d, TypeChambre t){
		throw new UnsupportedOperationException("pas encore implanté");
	}
	

	/**
	 * Cherche la proportion de chambres disponibles pour une date (réservées sur réservables).
	 * @param  d une date
	 * @return un entier entre 0 et 100
	 * @throws NullPointerException si un argument est null
	 */    
	public int getRatio(LocalDate d){
		throw new UnsupportedOperationException("pas encore implanté");
	}


	/**
	 * Cherche la proportion de chambres disponibles d'un certain type pour une date (réservées sur réservables).
	 * @param  d une date
	 * @param  t un type de chambre
	 * @return un entier entre 0 et 100
	 * @throws NullPointerException si un argument est null
	 */    
	public int getRatio(LocalDate d, TypeChambre t){
		throw new UnsupportedOperationException("pas encore implanté");
	}


	/**
	 * Cherche le nombre moyen de chambres disponibles entre deux date (réservées ou non), arrondies à l'entier inférieur.
	 * @param  d1 une date
	 * @param  d2 une date
	 * @return un entier
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalArgumentException si l'ordre temporel d1 avant d2 n'est pas respecté.
	 *
	 * Ne devrait pas retourner un entier négatif.
	 */    
	public int getDisponibles(LocalDate d1, LocalDate d2){
		throw new UnsupportedOperationException("pas encore implanté");
	}
	

	/**
	 * Cherche les réservations
	 * @param  d1 une date
	 * @param  d2 une date
	 * @param  t un type de chambre
	 * @return la ou les réservation(s) pour ce type de chambre entre les dates sous forme d'un ensemble
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalArgumentException si l'ordre temporel d1 avant d2 n'est pas respecté.
	 *
	 * Ne devrait pas retourner un objet null, par contre peut être un ensemble qui est vide.
	 */    
	public Set<Reservation> getReservation(LocalDate d1, LocalDate d2, TypeChambre t){
		throw new UnsupportedOperationException("pas encore implanté");
	}
	
	
	/**
	 * Cherche le <b>nombre moyen</b> de chambres disponibles d'un certain type entre deux date (réservées ou non), arrondies à l'entier inférieur.
	 * @param  d1 une date
	 * @param  d2 une date
	 * @param  t un type de chambre
	 * @return un entier
	 * @throws NullPointerException si un argument est null
	 * @throws IllegalArgumentException si l'ordre temporel d1 avant d2 n'est pas respecté.
	 *
	 * Ne devrait pas retourner un entier négatif.
	 */    
	public int getDisponibles(LocalDate d1, LocalDate d2, TypeChambre t){
		throw new UnsupportedOperationException("pas encore implanté");
	}
	

	/**
	 * Cherche la <b>proportion moyenne</b> de chambres disponibles pour une date (réservées sur réservables).
	 * @param  d1 une date
	 * @param  d2 une date
	 * @return un entier entre 0 et 100
	 * @throws NullPointerException si un argument est null
	 */    
	public int getRatio(LocalDate d1, LocalDate d2){
		throw new UnsupportedOperationException("pas encore implanté");
	}
	

	/**
	 * Cherche la <b>proportion moyenne</b> de chambres disponibles d'un certain type pour une date (réservées sur réservables).
	 * @param  d1 une date
	 * @param  d2 une date
	 * @param  t un type de chambre
	 * @return un entier entre 0 et 100
	 * @throws NullPointerException si un argument est null
	 */    
	public int getRatio(LocalDate d1, LocalDate d2, TypeChambre t){
		throw new UnsupportedOperationException("pas encore implanté");
	}

	@Override
	public Set<Reservation> getReservation(LocalDate d) {
		throw new UnsupportedOperationException("pas encore implanté");
	}
}