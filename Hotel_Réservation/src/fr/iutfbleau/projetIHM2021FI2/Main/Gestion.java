package fr.iutfbleau.projetIHM2021FI2.Main;
import fr.iutfbleau.projetIHM2021FI2.MP.*;
import fr.iutfbleau.projetIHM2021FI2.Vue.*;

/**
 * Classe qui contient le main
 */
public class Gestion {

	/**
	 * Main
	 * @param args
	 */
	public static void main(String[] args) {
		PrereservationFactoryMP booking = null;
		try {
			booking = new PrereservationFactoryMP();
		} catch (Exception e) {
			FenetreErreur fenErr = new FenetreErreur(e);
		}
		ReservationFactoryMP hotel = new ReservationFactoryMP();
		Fenetre fenetre = new Fenetre(booking, hotel);
	}
}
