package fr.iutfbleau.projetIHM2021FI2.BD;

import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.MP.*;
import org.mariadb.jdbc.*;
import java.sql.*;
import java.util.*;
import java.time.LocalDate;

/**
 * Classe chargée de gérer les requêtes ainsi que la connexion avec la base de données interne.
 */
public class ReservationBD {
	Connection bd;

	/**
	 * Constructeur qui établit la connexion avec la base de données interne.
	 * @throws SQLException si la connexion à la base de données a échoué.
	 * @throws ClassNotFoundException si le driver de mariadb n'a pas été trouvé.
	 */
	public ReservationBD() {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try {
				this.bd = DriverManager.getConnection(
					"jdbc:mariadb://dwarves.iut-fbleau.fr/ungar",
					"ungar", "mdpBDDungar2021");
				System.out.println("Connecté.");
			} catch(SQLException e) {
				System.err.println(e + "\nERROR : Connection failed");
			}
		} catch(ClassNotFoundException e2) {
			System.err.println("ERROR - Class not found");
		}			
	}

	/**
	 * Ferme la connexion avec la base de données interne.
	 * @throws SQLException si la connexion n'a pas pu être fermée.
	 */
	public void closeConnection() {
		try {
			this.bd.close();
			System.out.println("Connection BDHotel closed");
		} catch(SQLException e) {
			System.err.println(e + "\nConnection cannot be closed");
		}
	}

	/**
	 * Ajoute une réservation à la base de données interne
	 * @param  r une réservation
	 * @throws IllegalStateException si la chambre n'est pas disponible.
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 */
	public void createReservation(Reservation r) throws SQLException {
		String reference = r.getReference();
		LocalDate dateDebut = r.getDateDebut();
		int jours = r.getJours();
		Chambre chambre = r.getChambre();
		Client client = r.getClient();
		if(!clientInDB(client.getId())) {
			PreparedStatement ps = null;
			try {
				roomIsFree(chambre, dateDebut, jours);
				ps = this.bd.prepareStatement("INSERT INTO Client (id, prenom, nom) VALUES ("+client.getId()+", '"+client.getPrenom()+"', '"+client.getNom()+"');");
				ps.executeUpdate();
				ps.close();
			} catch(SQLException e) {
				System.err.println(e + "\nErreur création Client");
				ps.close();
				throw new SQLException("Accès à la base de données interne impossible");
			}
		}
		if(!reservationInDB(reference)) {
			PreparedStatement ps = null;
			try {
				ps = this.bd.prepareStatement("INSERT INTO Reservation (id, reference, debut, nuits, chambre, client) VALUES (NULL, '"+reference+"', '"+dateDebut+"', "+jours+", "+chambre.getNumero()+", "+client.getId()+");");
				ps.executeUpdate();
				ps.close();
			} catch(SQLException e) {
				System.err.println(e + "\nErreur création réservation");
				ps.close();
				throw new SQLException("Accès à la base de données interne impossible");
			}
		}
	}

	/**
	 * Vérifie si le client est déjà dans la base de données interne
	 * @param id identifiant du client
	 * @return vrai si déjà dans la base, faux sinon
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 */
	private boolean clientInDB(int id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = this.bd.prepareStatement("SELECT id FROM Client WHERE id="+id+";");
			rs = ps.executeQuery();
			if(rs.next()) {
				System.out.println("Client déjà dans la BD");
				rs.close();
				ps.close();
				return true;
			}
			rs.close();
			ps.close();
		} catch(SQLException e) {
			rs.close();
			ps.close();
			System.err.println(e + "Erreur ClientInDB");
			throw new SQLException("Accès à la base de données interne impossible");
		}
		return false;
	}

	/**
	 * Vérifie si la reservation est déjà dans la base de données interne
	 * @param reference référence de la réservation
	 * @return vrai si déjà dans la base, faux sinon
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 */
	public boolean reservationInDB(String reference) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = this.bd.prepareStatement("SELECT reference FROM Reservation WHERE reference='"+reference+"';");
			rs = ps.executeQuery();
			if(rs.next()) {
				System.out.println("Réservation déjà dans la BD");
				rs.close();
				ps.close();
				return true;
			}
			rs.close();
			ps.close();
		} catch(SQLException e) {
			rs.close();
			ps.close();
			System.err.println(e + "Erreur reservationInDB");
			throw new SQLException("Accès à la base de données interne impossible");
		}
		return false;
	}

	/**
	 * Vérifie si la chambre est disponible
	 * @param chambre chambre à vérifier
	 * @param dateDebut date de début de la réservation
	 * @param jours nombre de jours réservés
	 * @throws IllegalStateException si la chambre n'est pas disponible
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 */
	private void roomIsFree(Chambre chambre, LocalDate dateDebut, int jours) throws SQLException {
		LocalDate dateFin = dateDebut.plusDays(jours);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = this.bd.prepareStatement("SELECT id FROM Chambre WHERE categorie="+ChambreMP.getIdByTypeChambre(chambre.getType())+" AND id="+chambre.getNumero()+" AND id NOT IN(SELECT C.id FROM Chambre C, Reservation R WHERE C.id=R.chambre AND ('"+dateDebut+"'<DATE_ADD(R.debut, INTERVAL R.nuits DAY) AND '"+dateFin+"'>R.debut));");
			rs = ps.executeQuery();
			if(!rs.next()) {
				System.err.println("Chambre indisponible");
				rs.close();
				ps.close();
				throw new IllegalStateException("La chambre "+chambre.getNumero()+" n'est pas disponible du "+dateDebut+" au "+dateFin);
			}
			rs.close();
			ps.close();
		} catch(SQLException e) {
			rs.close();
			ps.close();
			throw new SQLException("Accès à la base de données interne impossible");
		}
	}

	/**
	 * Recherche une chambre adéquate à partir de
	 * @param  p une  préréservation 
	 * @return l'identifiant de la chambre
	 * @throws IllegalStateException si une chambre correspondant à cette Préréservation n'existe pas.
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 */
	public int getOneFreeRoom(Prereservation p) throws SQLException {
		LocalDate dateDebut = p.getDateDebut();
		LocalDate dateFin = dateDebut.plusDays(p.getJours());
		PreparedStatement ps = null;
		ResultSet rs = null;
		int room_id = -1;
		try {
			ps = this.bd.prepareStatement("SELECT id FROM Chambre WHERE categorie="+ChambreMP.getIdByTypeChambre(p.getTypeChambre())+" AND id NOT IN(SELECT C.id FROM Chambre C, Reservation R WHERE C.id=R.chambre AND ('"+dateDebut+"'<DATE_ADD(R.debut, INTERVAL R.nuits DAY) AND '"+dateFin+"'>R.debut)) LIMIT 1;");
			rs = ps.executeQuery();
			if(!rs.next()) {
				System.err.println("Aucune chambre de disponible");
				rs.close();
				ps.close();
				throw new IllegalStateException("Il n'existe aucune chambre avec "+p.getTypeChambre().toString()+" disponible du "+dateDebut+" au "+dateFin);
			}
			do {
				room_id = rs.getInt(1);
			} while(rs.next());
			rs.close();
			ps.close();
			return room_id;
		} catch(SQLException e) {
			rs.close();
			ps.close();
			throw new SQLException("Accès à la base de données interne impossible");
		}
	}

	/**
	 * Recherche les chambres adéquates à partir de
	 * @param p une préréservation
	 * @return les identifiants des chambres adéquates
	 * @throws IllegalStateException si aucune chambre n'est disponible
	 * @throws SQLException si l'accès à la base de données interne est impossible.
	 */
	public ArrayList<Integer> getFreeRoom(Prereservation p) throws SQLException {
		LocalDate dateDebut = p.getDateDebut();
		LocalDate dateFin = dateDebut.plusDays(p.getJours());
		ArrayList<Integer> room_ids = new ArrayList<>();
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT id FROM Chambre WHERE categorie="+ChambreMP.getIdByTypeChambre(p.getTypeChambre())+" AND id NOT IN(SELECT C.id FROM Chambre C, Reservation R WHERE C.id=R.chambre AND ('"+dateDebut+"'<DATE_ADD(R.debut, INTERVAL R.nuits DAY) AND '"+dateFin+"'>R.debut));");
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) {
				rs.close();
				ps.close();
				throw new IllegalStateException("Il n'existe aucune chambre avec "+p.getTypeChambre().toString()+" disponible du "+dateDebut+" au "+dateFin);
			}
			do {
				room_ids.add(rs.getInt(1));
			} while(rs.next());
			rs.close();
			ps.close();
			return room_ids;
		} catch(SQLException e) {
			System.out.println("Erreur getFreeRoom " + e);
			throw new SQLException("Accès à la base de données interne impossible");
		}
	}
}