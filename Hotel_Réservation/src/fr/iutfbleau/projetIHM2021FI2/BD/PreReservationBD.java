package fr.iutfbleau.projetIHM2021FI2.BD;

import fr.iutfbleau.projetIHM2021FI2.API.*;
import fr.iutfbleau.projetIHM2021FI2.MP.*;
import org.mariadb.jdbc.*;
import java.sql.*;
import java.util.*;
import java.lang.*;

/**
 * Classe chargée de gérer les requêtes ainsi que la connexion avec la base de données externe.
 */

public class PreReservationBD {
	Connection bd;

	/**
	 * Constructeur qui établit la connexion avec la base de données externe.
	 * @throws SQLException si la connexion à la base de données a échoué.
	 * @throws ClassNotFoundException si le driver de mariadb n'a pas été trouvé.
	 */
	public PreReservationBD() throws ClassNotFoundException, SQLException {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try {
				this.bd = DriverManager.getConnection(
					"jdbc:mariadb://dwarves.iut-fbleau.fr/projetihm",
					"projetihm", "mhitejorp");
				System.out.println("Connecté.");
			} catch(SQLException e) {
				System.err.println(e + "\nERROR : Connection failed");
				throw new SQLException("Connexion à la base de données impossible");
			}
		} catch(ClassNotFoundException e2) {
			System.err.println("ERROR - Class not found");
			throw new ClassNotFoundException("Le module de connexion à la base de données est introuvable");
		}	
	}

	/**
	 * Ferme la connexion avec la base de données externe.
	 * @throws SQLException si la connexion n'a pas pu être fermée.
	 */
	public void closeConnection() throws SQLException {
		try {
			this.bd.close();
			System.out.println("Connection Booking closed");
		} catch(SQLException e) {
			System.err.println(e + "\nConnection cannot be closed");
			throw new SQLException("La connexion à la base de données n'a pas pu être fermée");
		}
	}

	/**
	 * Recherche dans la base de données externes des préréservations par nom et prénom.
	 * @param n le nom
	 * @param p le prénom
	 * @return un set de préréservation
	 * @throws NullPointerException si un des deux arguments est vide.
	 * @throws IllegalStateException si aucune réservation n'a été trouvée.
	 * @throws SQLException si l'accès à la base de données externe est impossible.
	 */
	public Set<Prereservation> getPrereservations(String n, String p) throws SQLException {
		if(n.isEmpty() || p.isEmpty()) {
			throw new NullPointerException("Un nom ou un prénom ne peut être vide.");
		}
		Client clientDB = new ClientMP(0, "null", "null");
		Set<Prereservation> prereservations = new HashSet<Prereservation>();
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT reference, debut, nuits, categorie, C.id, prenom, nom FROM Reservation R, Client C WHERE R.client=C.id AND C.nom LIKE '%"+n+"%' AND C.prenom LIKE '%"+p+"%' ORDER BY C.id;");
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) {
				rs.close();
				ps.close();
				throw new IllegalStateException("Il n'y a pas de préréservation avec un Client ayant pour nom et prenom : " + n + " et " + p);
			}
			do {
				if(clientDB.getId() != rs.getInt(5)) {
					System.out.println("Création client set " + rs.getInt(5));
					clientDB = new ClientMP(rs.getInt(5), rs.getString(6), rs.getString(7));
				}
				prereservations.add(new PrereservationMP(rs.getString(1), rs.getDate(2).toLocalDate(), rs.getInt(3), ChambreMP.getTypeChambreById(rs.getInt(4)),  clientDB));
			} while(rs.next());
			rs.close();
			ps.close();
		} catch(SQLException e) {
			System.err.println(e);
			throw new SQLException("Accès à la base de données externe impossible");
		}
		printSet(prereservations);
		return prereservations;
	}

	/**
	 * Recherche dans la base de données externes des préréservations par référence.
	 * @param reference la référence du système de préréservation
	 * @return la préréservation
	 * @throws NullPointerException si la référence est vide.
	 * @throws IllegalStateException si aucune réservation n'a été trouvée.
	 * @throws SQLException si l'accès à la base de données externe est impossible.
	 */
	public Prereservation getPrereservation(String reference) throws SQLException {
		if(reference.isEmpty()) {
			throw new NullPointerException("La référence ne peut pas être vide.");
		}
		Client clientDB = new ClientMP(0, "null", "null");
		Prereservation prereservation = null;
		try {
			PreparedStatement ps = this.bd.prepareStatement("SELECT reference, debut, nuits, categorie, C.id, prenom, nom FROM Reservation R, Client C WHERE R.client=C.id AND R.reference='"+reference+"';");
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) {
				rs.close();
				ps.close();
				throw new IllegalStateException("Il n'y a pas de préréservation avec la référence : " + reference);
			}
			do {
				if(clientDB.getId() != rs.getInt(5)) {
					System.out.println("Création client");
					clientDB = new ClientMP(rs.getInt(5), rs.getString(6), rs.getString(7));
				}
				prereservation = new PrereservationMP(rs.getString(1), rs.getDate(2).toLocalDate(), rs.getInt(3), ChambreMP.getTypeChambreById(rs.getInt(4)), clientDB);
			} while(rs.next());
			rs.close();
			ps.close();
		} catch(SQLException e) {
			System.err.println(e);
			throw new SQLException("Accès à la base de données externe impossible");
		}
		System.out.println(prereservation);
		return prereservation;
	}

	/**
	 * Affiche de préréservation dans la console.
	 * @param prereservations Set de préréservations à afficher
	 */
	private static void printSet(Set<Prereservation> prereservations) {
		for(Prereservation pre:prereservations) {
			System.out.println(pre);
		}
	}
}